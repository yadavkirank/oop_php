<?php 
// older version autoload function
function __autoload($class)
{
	$class = strtolower($class);
	$the_path = "includes/{$class}.php";
    if (file_exists($the_path)) {
 		require_once($the_path);
 	}
 	else
 	{
 		die("This file name {$class}.php was not found!");
 	}
 }

// autoload function replaced by spl_autoload_register
function classAutoLoader($class)
{
	$class = strtolower($class);
	$the_path = 'includes/' . $class . '.php';
    if (is_file($the_path) && !class_exists($class)) {
    	require_once($the_path);
    }
    else
    {
    	die("This file named ".$class.".php was not found!");
    }
}

spl_autoload_register('classAutoLoader');
?>
