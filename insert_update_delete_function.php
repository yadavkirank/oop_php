<?php 
class User
{
	protected static $table_name="users";
	protected static $db_table_fields= array('username', 'password', 'first_name', 'last_name');
	public $id;
    protected function properties()
        {
            //return get_object_vars($this);
            $properties = array();
            foreach (self::$db_table_fields as $db_field) 
            {
                if (property_exists($this, $db_field)) {
                    $properties[$db_field] = $this->$db_field;
                }
            }
            return $properties;

        }
        protected function clean_properties()
        { 
            global $database;
            $clean_properties = array();
            foreach ($this->properties() as $key => $value) 
            {
                $clean_properties[$key] = $database->escape_string($value);
            }
            return $clean_properties;

        }

        public function save()
        {
            return isset($this->id) ? $this->update_user() : $this->create_user();
        }

        public function create()
        {
            global $database;
            $properties = $this->clean_properties();
            $sql = "iNSERT INTO " .self::$table_name. " (" . implode(",", array_keys($properties)) . ")";
            $sql .= " VALUES('". implode("','", array_values($properties)) ."')";


            if ($database->query($sql)) {
                $this->id = $database->the_inserted_id();
                return true;
            }
            else{
                return false;
            }
        }/* end of create user function */

        public function update()
        {
            global $database;
            $properties = $this->clean_properties();
            $properties_pairs = array();
            foreach ($properties as $key => $value) {
                $properties_pairs[] = "{$key}='{$value}'";
            }
            $sql = "UPDATE " .self::$table_name. " SET ";
            $sql .= implode(", ", $properties_pairs);
            $sql .= " WHERE id= ". $database->escape_string($this->id) ;

            $database->query($sql);
            return (mysqli_affected_rows($database->connection) == 1) ? true : false ;

        }/* end of Update user function */
        public function delete()
        {
            global $database;
            $sql = "DELETE  FROM " .self::$table_name. "  WHERE id= ".$database->escape_string($this->id);
            $database->query($sql);
            return (mysqli_affected_rows($database->connection) == 1) ? true : false ;

        }/* end of detete function */
}

?>
