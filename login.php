<?php require_once("includes/header.php"); ?>
<script type="text/javascript">
    function validate_login()
    {
   
        if (document.getElementById("user_name").value=="")
        {
        	alert( "Please provide user name!" );
            document.getElementById("user_name").focus() ;
            return false;
        }
        if (document.getElementById("user_pass").value=="")
        {
        	alert( "Please provide password!" );
            document.getElementById("user_name").focus() ;
            return false;
        }
        return true;
    }
</script> 
<?php if(!empty($session->is_signed_in())){  redirect("index.php");} ?>
<?php
if (isset($_POST['submit']))
{
	$username=trim($_POST['username']);
	$password=trim($_POST['password']);
	


	/// Method for check databse user

	$user_found = User::verify_user($username, $password);
	if($user_found) 
	{
		$session->login($user_found);
		//header("Location:index.php");
		redirect("index.php");
		
	}
	else
	{
		$the_message="Your password or username are incorrect";
	}
}
else
{
	$the_message="";
	$username="";
	$password="";
}
?>

<div class="col-md-4 col-md-offset-3">

<h4 class="bg-danger"><?php echo $the_message; ?></h4>
	
<form  id="login-id" action="" method="post" onsubmit = "return validate_login()">
	
<div class="form-group">
	<label for="username">Username</label>
	<input type="text" class="form-control" id="user_name" name="username" value="<?php echo htmlentities($username); ?>" >

</div>

<div class="form-group">
	<label for="password">Password</label>
	<input type="password" class="form-control" id="user_pass" name="password" value="<?php echo htmlentities($password); ?>">
	
</div>


<div class="form-group">
<input type="submit" name="submit" value="Submit" class="btn btn-primary">

</div>


</form>


</div>
